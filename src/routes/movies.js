const { Router } = require('express')
const router = Router()
const _ = require ('underscore')

const movies = require(`../sample.json`)

router.get('/', (req,res)=>{
  res.json(movies)
})

router.post('/', (req,res)=>{
  const { title, director, year, rating } = req.body
  if(title && director && year && rating) {
    const id = movies.length+1
    const newMovie = {id, ...req.body}
    movies.push(newMovie)
    res.json(movies)
  } else {
  res.send('somenthing went wrong')
  }
})

router.put('/:id', (req,res)=>{
  const { id } = req.params
  const { title, director, year, rating } = req.body
  if( title && director && year && rating) {
    _.each(movies,(movie,i)=>{
      if(movie.id == id){
        movie.title = title
        movie.director = director
        movie.year = year
        movie.rating = rating
      }
    })
    res.json(movies)
  } else {
    res.status(500).send('Something went wrong')
  }

})

router.delete(`/:id`, (req,res)=>{
  const { id } = req.params
  console.log ('Antes',movies)
  _.each(movies,(movie, i)=>{
    if(movie.id == id){
      movies.splice(i,1)
    }
  })
  console.log('Después',movies)
  res.send('eliminado')
})

module.exports = router
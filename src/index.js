const express = require ('express');
const morgan = require('morgan');

const app = express();

const routerIndex = require(`./routes/index.js`)
const routerMovies = require(`./routes/movies.js`)
const routerUsers = require('./routes/user')

//settings
app.set('port', 5000)
app.set('json spaces', 2)

//Server starter
app.listen(app.get('port'), ()=>{
  console.log(`Server on port ${app.get(`port`)}`)
})

//Middlewares
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))

//Routes
app.use(`/api`, routerIndex )
app.use(`/api/movies`, routerMovies)
app.use(`/api/users`, routerUsers)
